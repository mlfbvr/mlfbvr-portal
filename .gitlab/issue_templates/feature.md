# Feature request

- [ ] Is it an enhancement of an existing feature ?

_Please link relevant features below :_

## Description

_Please describe the context and the feature you want to see here_

### Design

_Please explain the worflow of the feature, linking screens if relevant, or with a diagram, such as a sequence diagram or an user journey diagram_

_You can use [mermaid](https://docs.gitlab.com/ee/user/markdown.html#mermaid) for example :_

```mermaid
sequenceDiagram
    Alice->>Bob: Hello Bob, how are you?
    alt is sick
        Bob->>Alice: Not so good :(
    else is well
        Bob->>Alice: Feeling fresh like a daisy
    end
    opt Extra response
        Bob->>Alice: Thanks for asking
    end
```

### Acceptance tests

_Please describe test scenarios to ensure the feature works as attended_

_They will be used for e2e testing_
