#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER $CACHETHQ_POSTGRES_USER WITH PASSWORD '$CACHETHQ_POSTGRES_PASSWORD';
    CREATE DATABASE $CACHETHQ_DATABASE;
    GRANT ALL PRIVILEGES ON DATABASE $CACHETHQ_DATABASE TO $CACHETHQ_POSTGRES_USER;
EOSQL
