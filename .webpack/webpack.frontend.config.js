const nrwlConfig = require('@nrwl/react/plugins/webpack.js');

module.exports = (config) => {
  nrwlConfig(config);

  config.module.rules.push({
    test: /\.css$/,
    use: [
      {
        loader: 'postcss-loader',
      },
    ],
  });

  if (config.devServer) {
    config.devServer.host = '0.0.0.0';
    config.devServer.disableHostCheck = true;
  }

  return config;
};
