import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthenticatedHttpGuard } from '@mlfbvr/backend/feature/auth';

import { AppService } from './app.service';

@UseGuards(AuthenticatedHttpGuard)
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }
}
