import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { AuthModule } from '@mlfbvr/backend/feature/auth';
import config, { schema } from '@mlfbvr/api-gateway/config/env';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import AppClientsModule from './services/client.module';

@Module({
  imports: [
    AppClientsModule,
    AuthModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [() => config],
      validationSchema: schema,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
