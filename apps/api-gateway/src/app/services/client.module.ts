import { ClientsModule } from '@nestjs/microservices';
import { SERVICES } from '@mlfbvr/shared/config/services';

export default ClientsModule.register(SERVICES);
