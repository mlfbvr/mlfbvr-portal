import { NestFactory } from '@nestjs/core';
import { bootstrapLogger } from '@mlfbvr/backend/feature/logger';

import { AppModule } from './app/app.module';

import startHttpServer from './services/http/server/start';
import bootstrapHttpServer from './services/http/server/bootstrap';
import bootstrapSwagger from './services/swagger/bootstrap';

export async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  bootstrapLogger(app);
  bootstrapHttpServer(app);
  bootstrapSwagger(app);
  await startHttpServer(app);
}
