import * as Joi from 'joi';
import { getEnvironmentVariable } from '@mlfbvr/utils/env';

export type ApiGatewayConfig = {
  app: {
    globalPrefix: string;
    version: string;
    port: number;
  };
};

export const schema = Joi.object({
  app: Joi.object({
    version: Joi.string().required(),
    port: Joi.number().required(),
  }),
});

export default {
  app: {
    globalPrefix: 'api',
    version: getEnvironmentVariable('npm_package_version'),
    port: getEnvironmentVariable('API_GATEWAY_PORT', { transform: (s) => +s }),
  },
} as ApiGatewayConfig;
