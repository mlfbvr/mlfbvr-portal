import { AppLogger } from '@mlfbvr/backend/feature/logger';
import { bootstrap } from './bootstrap';

bootstrap().catch((err) => {
  AppLogger.error(err.message, err.stackTrace);
});
