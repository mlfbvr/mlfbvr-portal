import React from 'react';
import { render } from '@testing-library/react';

import { BrowserRouter } from 'react-router-dom';

import Root from './root';

describe('App', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <BrowserRouter>
        <Root />
      </BrowserRouter>
    );

    expect(baseElement).toBeTruthy();
  });
});
