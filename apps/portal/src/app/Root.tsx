import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import { Layout } from '@mlfbvr/frontend/ui/layout/layout';
import { AuthProvider } from '@mlfbvr/frontend/auth/feature';

export function Root() {
  return (
    <AuthProvider>
      <BrowserRouter>
        <Layout />
      </BrowserRouter>
    </AuthProvider>
  );
}

export default Root;
