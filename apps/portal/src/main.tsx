import React from 'react';
import ReactDOM from 'react-dom';

import 'tailwindcss/tailwind.css';

import Root from './app/root';

ReactDOM.render(
  <React.StrictMode>
    <Root />
  </React.StrictMode>,
  document.getElementById('root')
);
