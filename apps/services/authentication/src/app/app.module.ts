import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { AuthController, AuthModule } from '@mlfbvr/backend/feature/auth';
import config, { schema } from '../config/env';

@Module({
  imports: [
    AuthModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [() => config],
      validationSchema: schema,
    }),
  ],
  controllers: [AuthController],
  providers: [],
})
export class AppModule {}
