import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';

import startHttpServer from './services/http/server/start';
import bootstrapHttpServer from './services/http/server/bootstrap';
import bootstrapSwagger from './services/swagger/bootstrap';
import { bootstrapPassport } from '@mlfbvr/backend/feature/auth';
import { bootstrapSession } from '@mlfbvr/backend/feature/session';
import { bootstrapLogger } from '@mlfbvr/backend/feature/logger';

export async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  bootstrapLogger(app);
  bootstrapHttpServer(app);
  bootstrapSession(app);
  bootstrapPassport(app);

  bootstrapSwagger(app);
  await startHttpServer(app);
}
