import * as Joi from 'joi';
import { getEnvironmentVariable } from '@mlfbvr/utils/env';

export type AuthenticationServiceConfig = {
  app: {
    globalPrefix: string;
    version: string;
    port: number;
  };
};

export const schema = Joi.object({
  app: Joi.object({
    version: Joi.string().required(),
    port: Joi.number().required(),
  }),
});

export default {
  app: {
    globalPrefix: 'auth',
    version: getEnvironmentVariable('$npm_package_version'),
    port: getEnvironmentVariable('AUTHENTICATION_SERVICE_PORT', {
      transform: (s) => +s,
    }),
  },
} as AuthenticationServiceConfig;
