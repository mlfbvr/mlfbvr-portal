import { AppLogger } from '@mlfbvr/backend/feature/logger';
import config from '../../../config/env';

export default (app) => {
  const port = config.app.port;

  app.setGlobalPrefix(config.app.globalPrefix);

  return app.listen(port, () => {
    AppLogger.log('Listening at http://localhost:' + port);
  });
};
