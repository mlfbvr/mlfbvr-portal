import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import config from '../../config/env';

export default (app) => {
  const options = new DocumentBuilder()
    .setTitle('MLFBVR API Gateway')
    .setDescription('Doc')
    .setVersion(config.app.version)
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup(config.app.globalPrefix, app, document);
};
