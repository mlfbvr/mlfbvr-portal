import { addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';

import 'tailwindcss/tailwind.css';

addDecorator(withKnobs);
