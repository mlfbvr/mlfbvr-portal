version: '3.8'

x-rmq-env: &rmq-env
  RABBITMQ_USERNAME: $RABBITMQ_USERNAME
  RABBITMQ_PASSWORD: $RABBITMQ_PASSWORD
  RABBITMQ_VHOST: $RABBITMQ_VHOST

x-postgresql-env: &postgresql-env
  POSTGRES_USER: $POSTGRES_USER
  POSTGRES_PASSWORD: $POSTGRES_PASSWORD
  POSTGRES_DB: $POSTGRES_DB

x-postgresql-keycloak-env: &postgresql-keycloak-env
  KEYCLOAK_DATABASE: $KEYCLOAK_DATABASE
  KEYCLOAK_SCHEMA: $KEYCLOAK_SCHEMA
  KEYCLOAK_POSTGRES_USER: $KEYCLOAK_POSTGRES_USER
  KEYCLOAK_POSTGRES_PASSWORD: $KEYCLOAK_POSTGRES_PASSWORD

x-postgres-cachethq-env: &postgresql-cachethq-env
  CACHETHQ_POSTGRES_USER: $CACHETHQ_POSTGRES_USER
  CACHETHQ_POSTGRES_PASSWORD: $CACHETHQ_POSTGRES_PASSWORD
  CACHETHQ_DATABASE: $CACHETHQ_DATABASE

x-keycloak-env: &keycloak-env
  KEYCLOAK_USER: $KEYCLOAK_ADMIN_USER
  KEYCLOAK_PASSWORD: $KEYCLOAK_ADMIN_PASSWORD
  DB_USER: $KEYCLOAK_POSTGRES_USER
  DB_PASSWORD: $KEYCLOAK_POSTGRES_PASSWORD
  DB_DATABASE: $KEYCLOAK_DATABASE
  DB_SCHEMA: $KEYCLOAK_SCHEMA

services:
  rmq:
    image: bitnami/rabbitmq:3.8.9
    ports:
      - 9000:5672
      - 9001:15672
    environment:
      <<: *rmq-env

  postgresql:
    image: postgres:13
    environment:
      <<: *postgresql-env
      <<: *postgresql-keycloak-env
      <<: *postgresql-cachethq-env

  keycloak:
    image: quay.io/keycloak/keycloak:11.0.3
    ports:
      - 9002:8080
    depends_on:
      - postgresql
    links:
      - postgresql:postgresql
    environment:
      <<: *keycloak-env
      DB_ADDR: postgresql
      DB_VENDOR: postgres
      PROXY_ADDRESS_FORWARDING: 'true'
      KEYCLOAK_HOSTNAME: auth.localhost
      KEYCLOAK_FRONTEND_URL: http://auth.localhost/auth

  redis:
    image: redis
    ports:
      - 6379:6379

  ingress:
    image: nginx:latest
    depends_on:
      - cachethq
    links:
      - cachethq:cachethq
    ports:
      - 80:3000
    volumes:
      - .local/docker/ingress/templates:/etc/nginx/templates
    environment:
      NGINX_PORT: 3000
      NGINX_DOMAIN: localhost
      API_GATEWAY_URI: "host.docker.internal:$API_GATEWAY_PORT"
      AUTH_SERVICE_URI: "host.docker.internal:$AUTHENTICATION_SERVICE_PORT"
      PORTAL_URI: "host.docker.internal:4200"
      IDENTITY_PROVIDER_URI: "host.docker.internal:9002"
      STATUS_URI: "cachethq:8000"

  cachethq:
    image: cachethq/docker:2.3.15
    ports:
      - 9003:8000
    depends_on:
      - postgresql
      - redis
    links:
      - postgresql:postgresql
      - redis:redis
    environment:
      DB_DRIVER: pgsql
      DB_HOST: postgresql
      DB_DATABASE: $CACHETHQ_DATABASE
      DB_USERNAME: $CACHETHQ_POSTGRES_USER
      DB_PASSWORD: $CACHETHQ_POSTGRES_PASSWORD
      APP_KEY: base64:0LSxsF00wXPzV0kDukLd4J8tiP5AISKAncST8Qdu5JE=

  api-gateway:
    build:
      context: .
      dockerfile: service.Dockerfile
      args:
        PACKAGE_PATH: apps/api-gateway
