import * as redis from 'redis';
import { buildRedisConnectionString } from './utils';

const REDIS_CLIENTS = {};

export const getRedisClient = (db: number | string = 0) => {
  if (!REDIS_CLIENTS[db]) {
    REDIS_CLIENTS[db] = redis.createClient(buildRedisConnectionString(db));
  }
  return REDIS_CLIENTS[db];
};
