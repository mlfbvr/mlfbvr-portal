import { getEnvironmentVariable } from '@mlfbvr/utils/env';
import { buildRedisConnectionString } from './utils';

jest.mock('@mlfbvr/utils/env');

describe('libs/backend/data-access/redis/lib/utils', () => {
  describe('buildRedisConnectionString', () => {
    it.each`
      REDIS_URL                         | REDIS_USER   | REDIS_PASSWORD | db   | expected
      ${'redis://host:9000/?yolo=swag'} | ${undefined} | ${undefined}   | ${0} | ${'redis://host:9000/0?yolo=swag'}
      ${'host:9000/?yolo=swag'}         | ${undefined} | ${undefined}   | ${1} | ${'//host:9000/1?yolo=swag'}
      ${'redis://host:9000/?yolo=swag'} | ${'user'}    | ${undefined}   | ${2} | ${'redis://user@host:9000/2?yolo=swag'}
      ${'host:9000/?yolo=swag'}         | ${'user'}    | ${undefined}   | ${3} | ${'//user@host:9000/3?yolo=swag'}
      ${'redis://host:9000/?yolo=swag'} | ${'user'}    | ${'password'}  | ${4} | ${'redis://user:password@host:9000/4?yolo=swag'}
      ${'host:9000/?yolo=swag'}         | ${'user'}    | ${'password'}  | ${5} | ${'//user:password@host:9000/5?yolo=swag'}
    `(
      'should return the redis connection string if nothing was added',
      ({ REDIS_URL, REDIS_USER, REDIS_PASSWORD, db, expected }) => {
        const config = {
          REDIS_URL,
          REDIS_USER,
          REDIS_PASSWORD,
        };

        (getEnvironmentVariable as jest.Mock).mockImplementation(
          (k) => config[k]
        );
        expect(buildRedisConnectionString(db)).toBe(expected);
      }
    );
  });
});
