import { getEnvironmentVariable } from '@mlfbvr/utils/env';

import { ensureProtocol, removePlaceholderProtocol } from '@mlfbvr/utils/url';

export const buildRedisConnectionString = (db: number | string = 0) => {
  const redisUrl: string = getEnvironmentVariable('REDIS_URL', {
    shouldThrow: true,
  });

  const url = new URL(ensureProtocol(redisUrl));

  url.username = url.username || getEnvironmentVariable('REDIS_USER') || '';
  url.password = url.password || getEnvironmentVariable('REDIS_PASSWORD') || '';
  url.pathname = `/${db}`;

  return removePlaceholderProtocol(url.toString());
};
