export * from './lib/auth.module';
export { AuthenticatedHttpGuard } from './lib/guards/authenticated-http.guard.service';
export { default as bootstrapPassport } from './lib/bootstrap';
