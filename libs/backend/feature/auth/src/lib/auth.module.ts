import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

import OidcClientFactory, { OidcClient } from './clients/oidc.client';
import { AuthController } from './controllers/auth.controller';
import { OidcStrategy } from './strategies/oidc.strategy';
import { SessionSerializer } from './serializers/session.serializer';
import { AuthService } from './services/auth.service';

export { AuthController };

@Module({
  imports: [PassportModule],
  controllers: [],
  providers: [
    {
      provide: OidcClient,
      useFactory: OidcClientFactory,
    },
    AuthService,
    OidcStrategy,
    SessionSerializer,
  ],
  exports: [AuthService],
})
export class AuthModule {}
