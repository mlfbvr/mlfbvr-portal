import { Client, Issuer } from 'openid-client';

import config from '../config/env';

export const OidcClient = Symbol.for('OidcClient');

export default async (): Promise<Client> => {
  const { Client } = await Issuer.discover(
    `${config.issuer}/.well-known/openid-configuration`
  );
  return new Client({
    client_id: config.clientId,
    client_secret: config.clientSecret,
  });
};
