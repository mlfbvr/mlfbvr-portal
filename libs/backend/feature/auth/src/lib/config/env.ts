import { getEnvironmentVariable } from '@mlfbvr/utils/env';

export type AuthConfig = {
  issuer: string;
  clientId: string;
  clientSecret: string;
  scope: string[];
  redirectUri: string;
  postLogoutRedirectUri: string;
};

export default {
  issuer: getEnvironmentVariable('OAUTH2_CLIENT_PROVIDER_OIDC_ISSUER', {
    shouldThrow: true,
  }),
  clientId: getEnvironmentVariable(
    'OAUTH2_CLIENT_REGISTRATION_LOGIN_CLIENT_ID',
    { shouldThrow: true }
  ),
  clientSecret: getEnvironmentVariable(
    'OAUTH2_CLIENT_REGISTRATION_LOGIN_CLIENT_SECRET',
    { shouldThrow: true }
  ),
  scope: getEnvironmentVariable('OAUTH2_CLIENT_REGISTRATION_LOGIN_SCOPE', {
    shouldThrow: true,
    transform: (v = '') => v.split(','),
  }),
  redirectUri: getEnvironmentVariable(
    'OAUTH2_CLIENT_REGISTRATION_LOGIN_REDIRECT_URI',
    { shouldThrow: true }
  ),
  postLogoutRedirectUri: getEnvironmentVariable(
    'OAUTH2_CLIENT_POST_LOGOUT_REDIRECT_URI',
    { shouldThrow: true }
  ),
} as AuthConfig;
