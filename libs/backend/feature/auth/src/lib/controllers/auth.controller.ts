import { promisify } from 'util';

import { Controller, Get, Next, Req, Res, UseGuards } from '@nestjs/common';

import { AuthService } from '../services/auth.service';
import { LoginGuard } from '../guards/login.guard';

@Controller()
export class AuthController {
  constructor(private readonly service: AuthService) {}

  @UseGuards(LoginGuard)
  @Get('/user/login')
  public login(@Next() next) {
    next();
  }

  @UseGuards(LoginGuard)
  @Get('/user/callback')
  public callback(@Req() req, @Res() res) {
    res.redirect(`/#token=${req.user.access_token}`);
  }

  @Get('/user/logout')
  public async logout(@Req() req, @Res() res) {
    const { id_token } = (req.user || {}) as { id_token: string | undefined };

    req.logout();

    if (req.session) {
      try {
        const destroy = promisify(req.session.destroy.bind(req.session));
        await destroy();
      } catch (e) {
        // TODO
      }

      const logoutUri = this.service.getLogoutUri(id_token);
      if (logoutUri) {
        return res.redirect(logoutUri);
      }
    }

    res.redirect('/');
  }
}
