import { ExecutionContext, Injectable, CanActivate } from '@nestjs/common';
import { extractJwtFromBearer } from '../utils/headers';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthenticatedHttpGuard implements CanActivate {
  constructor(private service: AuthService) {}

  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();

    const { access_token } = request.user || {};
    const bearer = extractJwtFromBearer(request.headers.Authorization);

    // Validate token
    const { active: isTokenActive } = await this.service.validateToken(
      access_token || bearer
    );

    return isTokenActive;
  }
}
