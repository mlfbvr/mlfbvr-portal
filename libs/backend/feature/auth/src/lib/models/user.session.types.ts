import { TokenSet, UserinfoResponse } from 'openid-client';

export type UserSession = TokenSet & { userinfo: UserinfoResponse };
