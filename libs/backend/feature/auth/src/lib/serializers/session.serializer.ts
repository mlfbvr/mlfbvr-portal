import { PassportSerializer } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { UserSession } from '../models/user.session.types';
@Injectable()
export class SessionSerializer extends PassportSerializer {
  serializeUser(
    user: UserSession,
    done: (err: Error, user: UserSession) => void
  ): void {
    done(null, user);
  }
  deserializeUser(
    payload: string,
    done: (err: Error, payload: string) => void
  ): void {
    done(null, payload);
  }
}
