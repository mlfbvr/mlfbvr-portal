import { Inject, Injectable } from '@nestjs/common';
import { Client, IntrospectionResponse } from 'openid-client';
import { OidcClient } from '../clients/oidc.client';
import config from '../config/env';

@Injectable()
export class AuthService {
  constructor(@Inject(OidcClient) private client: Client) {}

  getClient(): Client {
    return this.client;
  }

  buildLogoutUri(endpoint: string, id_token: string | undefined): string {
    const url = new URL(endpoint);

    url.searchParams.append(
      'post_logout_redirect_uri',
      config.postLogoutRedirectUri
    );

    if (id_token) {
      url.searchParams.append('id_token_hint', id_token);
    }

    return url.href;
  }

  getLogoutUri(id_token: string | undefined): string | undefined {
    const { end_session_endpoint } = this.client.issuer.metadata;
    return (
      end_session_endpoint &&
      this.buildLogoutUri(end_session_endpoint, id_token)
    );
  }

  validateToken(token: string): Promise<IntrospectionResponse> {
    return this.getClient().introspect(token);
  }
}
