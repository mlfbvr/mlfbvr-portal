import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

import { Client, Strategy, TokenSet, UserinfoResponse } from 'openid-client';
import { UserSession } from '../models/user.session.types';
import { AuthService } from '../services/auth.service';

import config from '../config/env';

@Injectable()
export class OidcStrategy extends PassportStrategy(Strategy, 'oidc') {
  constructor(private readonly service: AuthService) {
    super({
      client: service.getClient(),
      params: {
        redirect_uri: config.redirectUri,
        scope: config.scope,
      },
      passReqToCallback: false,
      usePKCE: false,
    });
  }

  async validate(tokenset: TokenSet): Promise<UserSession> {
    try {
      const client = this.service.getClient() as Client;
      const userinfo: UserinfoResponse = await client.userinfo(tokenset);

      return {
        ...tokenset,
        userinfo,
      } as UserSession;
    } catch (err) {
      throw new UnauthorizedException();
    }
  }
}
