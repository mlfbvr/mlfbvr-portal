import * as faker from 'faker';
import { extractJwtFromBearer } from './headers';

describe('libs/backend/feature/auth/utils', () => {
  describe('extractJwtFromBearer', () => {
    const token = faker.random.word();
    it.each`
      header               | token
      ${'Bearer token'}    | ${'token'}
      ${`Bearer ${token}`} | ${token}
      ${`Bearer`}          | ${''}
      ${``}                | ${''}
    `('extracts correctly the jwt from the bearer', ({ token, header }) => {
      expect(extractJwtFromBearer(header)).toBe(token);
    });
  });
});
