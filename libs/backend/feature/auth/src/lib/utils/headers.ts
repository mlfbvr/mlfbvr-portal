const BEARER_TOKEN_REGEXP = /Bearer\s+([^\s]*)/i;

export const extractJwtFromBearer = (header: string): string => {
  const [, token] = header.match(BEARER_TOKEN_REGEXP) || [];
  return token || '';
};
