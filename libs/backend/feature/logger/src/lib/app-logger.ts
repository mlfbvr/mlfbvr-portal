import { Logger } from '@nestjs/common';
import { Sentry } from './client';

export class AppLogger extends Logger {
  static _instance: AppLogger = null;

  static get instance(): AppLogger {
    if (!AppLogger._instance) {
      AppLogger._instance = new AppLogger();
    }
    return AppLogger._instance;
  }

  log(message: string, context?: string) {
    // Sentry.captureMessage(message, Sentry.Severity.Log);
    super.log(message, context);
  }

  error(message: string, trace?: string, context?: string) {
    Sentry.captureMessage(message, Sentry.Severity.Error);
    super.error(message, trace, context);
  }

  warn(message: string, context?: string) {
    Sentry.captureMessage(message, Sentry.Severity.Warning);
    super.warn(message, context);
  }

  debug(message: string, context?: string) {
    // Sentry.captureMessage(message, Sentry.Severity.Debug);
    super.debug(message, context);
  }

  verbose(message: string, context?: string) {
    // Sentry.captureMessage(message, Sentry.Severity.Info);
    super.verbose(message, context);
  }

  captureException(error: Error) {
    Sentry.captureException(error);
  }

  static log(message: string, context?: string) {
    AppLogger.instance.log(message, context);
  }

  static error(message: string, trace?: string, context?: string) {
    AppLogger.instance.error(message, trace, context);
  }

  static warn(message: string, context?: string) {
    AppLogger.instance.warn(message, context);
  }

  static debug(message: string, context?: string) {
    AppLogger.instance.debug(message, context);
  }

  static verbose(message: string, context?: string) {
    AppLogger.instance.verbose(message, context);
  }

  static captureException(error: Error) {
    AppLogger.instance.captureException(error);
  }
}
