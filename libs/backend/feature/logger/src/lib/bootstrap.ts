import { INestApplication } from '@nestjs/common';
import { AppLogger } from './app-logger';

export const bootstrapLogger = (app: INestApplication) => {
  app.useLogger(AppLogger.instance);
};
