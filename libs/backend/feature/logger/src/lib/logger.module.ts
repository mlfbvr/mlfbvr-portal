import { Module } from '@nestjs/common';
import { AppLogger } from './app-logger';

@Module({
  controllers: [],
  providers: [AppLogger],
  exports: [AppLogger],
})
export class LoggerModule {}
