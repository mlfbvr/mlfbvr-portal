import * as session from 'express-session';
import * as connectRedis from 'connect-redis';

import { getEnvironmentVariable } from '@mlfbvr/utils/env';
import { getRedisClient } from '@mlfbvr/backend/data-access/redis';

export const bootstrapSession = (app) => {
  const RedisStore = connectRedis(session);

  app.use(
    session({
      store: new RedisStore({
        client: getRedisClient(getEnvironmentVariable('API_SESSION_REDIS_DB')),
      }),
      secret: getEnvironmentVariable('API_SESSION_SECRET', {
        shouldThrow: true,
      }),
      resave: true,
      rolling: true,
      saveUninitialized: true,
      cookie: {
        httpOnly: true,
        maxAge: 30 * 60 * 1000,
      },
    })
  );
};
