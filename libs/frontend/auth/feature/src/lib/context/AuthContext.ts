import React from 'react';
import { noop } from 'lodash';

export type AuthContextType = {
  isAuthenticated: boolean;
  accessToken: string;
  logout: () => void;
};

export const AuthContext = React.createContext<AuthContextType>({
  isAuthenticated: false,
  accessToken: '',
  logout: noop,
});
