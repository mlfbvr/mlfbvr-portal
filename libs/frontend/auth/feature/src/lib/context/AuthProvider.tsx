import React, { useCallback, useMemo } from 'react';
import { node } from 'prop-types';

import { useLocalStorage } from '@mlfbvr/frontend/feature/local-storage';

import { AuthContext } from './AuthContext';
import { getAccessTokenFromHash } from '../token';
import { InferPropTypes } from '@mlfbvr/shared/ui/proptypes';
import { useAccessTokenStorage } from '../token/useAccessTokenStorage';

const propTypes = {
  children: node,
};

const defaultProps = {
  children: null,
};

export type AuthProviderProps = InferPropTypes<
  typeof propTypes,
  typeof defaultProps
>;

export const AuthProviderBase = ({ children }: AuthProviderProps) => {
  const [accessToken, , clearAccessToken] = useAccessTokenStorage();

  const isAuthenticated = !!accessToken;

  const logout = useCallback(() => {
    clearAccessToken();
  }, [clearAccessToken]);

  const context = useMemo(
    () => ({
      isAuthenticated,
      accessToken,
      logout,
    }),
    [isAuthenticated, accessToken, logout]
  );

  return (
    <AuthContext.Provider value={context}>{children}</AuthContext.Provider>
  );
};

AuthProviderBase.displayName = 'AuthProvider';
AuthProviderBase.propTypes = propTypes;
AuthProviderBase.defaultProps = defaultProps;

export const AuthProvider = React.memo(AuthProviderBase);
