import { useContext } from 'react';
import { AuthContext } from '../context/AuthContext';

export const useAccessToken = () => {
  const { accessToken } = useContext(AuthContext);

  return accessToken;
};
