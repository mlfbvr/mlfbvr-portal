import * as fc from 'fast-check';
import { getAccessTokenFromHash } from './index';

describe('frontend/auth/feature/token', () => {
  describe('getAccessTokenFromHash', () => {
    it('should retrieve the token from the hash', () => {
      fc.assert(
        fc.property(
          fc.string().map((s) => encodeURIComponent(s)),
          (token: string) => {
            window.location.hash = `token=${token}`;

            expect(getAccessTokenFromHash()).toBe(token);
            expect(window.location.hash).toBe('');
          }
        )
      );
    });

    it('should not change window.location.hash if there is no token in it', () => {
      fc.assert(
        fc.property(
          fc
            .string()
            .filter((s) => s.length > 0)
            .map((s) => encodeURIComponent(s)),
          (data: string) => {
            window.location.hash = data;

            expect(getAccessTokenFromHash()).toBe('');
            expect(window.location.hash).toBe(`#${data}`);
          }
        )
      );
    });

    it('should not change window.location.hash if there is no hash', () => {
      window.location.hash = '';
      expect(getAccessTokenFromHash()).toBe('');
      expect(window.location.hash).toBe('');
    });
  });
});
