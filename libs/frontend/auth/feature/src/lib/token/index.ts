export const getAccessTokenFromHash = (): string => {
  const { hash } = window.location;

  if (!hash?.startsWith('#token=')) {
    return '';
  }
  const [, token] = hash.match(/^#token=(.*)/);

  window.location.hash = '';

  return token;
};
