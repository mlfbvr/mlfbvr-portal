import { useLocalStorage } from '@mlfbvr/frontend/feature/local-storage';
import { getAccessTokenFromHash } from './index';

export const useAccessTokenStorage = () => {
  return useLocalStorage<string>(
    'lib:auth:feature:access-token',
    getAccessTokenFromHash()
  );
};
