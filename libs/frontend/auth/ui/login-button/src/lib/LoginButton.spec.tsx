import React from 'react';
import { render } from '@testing-library/react';

import LoginButton from './LoginButton';

describe('FrontendAuthUiLoginButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<LoginButton />);
    expect(baseElement).toBeTruthy();
  });
});
