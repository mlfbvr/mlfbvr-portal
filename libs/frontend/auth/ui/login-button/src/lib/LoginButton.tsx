import React from 'react';
import { InferPropTypes } from '@mlfbvr/shared/ui/proptypes';
import { Button, ButtonTypes } from '@mlfbvr/shared/ui/design-system';
import { useAuthContext } from '@mlfbvr/frontend/auth/feature';
import { AUTH_ROUTES } from '@mlfbvr/shared/config/routes';

const propTypes = {};
const defaultProps = {};

export type LoginButtonProps = InferPropTypes<
  typeof propTypes,
  typeof defaultProps
>;

export const LoginButtonBase = () => {
  const { isAuthenticated, logout } = useAuthContext();

  if (isAuthenticated) {
    return (
      <button onClickCapture={logout}>
        <a href={AUTH_ROUTES.LOGOUT}>
          <Button type={ButtonTypes.PRIMARY}>Se déconnecter</Button>
        </a>
      </button>
    );
  }

  return (
    <a href={AUTH_ROUTES.LOGIN}>
      <Button type={ButtonTypes.PRIMARY}>Se connecter</Button>
    </a>
  );
};

export const LoginButton = React.memo(LoginButtonBase);
