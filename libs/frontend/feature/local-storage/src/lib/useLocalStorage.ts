import { useCallback, useState } from 'react';

export const useLocalStorage = <T>(
  key: string,
  initialValue: T
): [T, (value: T | ((v: T) => T)) => void, () => void] => {
  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      const item = window.localStorage.getItem(key);
      if (item) {
        return JSON.parse(item);
      }
      if (initialValue) {
        window.localStorage.setItem(key, JSON.stringify(initialValue));
      }
      return initialValue;
    } catch (error) {
      console.error(error);
      return initialValue;
    }
  });

  const setValue = useCallback(
    (value: T | ((v: T) => T)) => {
      try {
        const v = value instanceof Function ? value(storedValue) : value;
        setStoredValue(v);
        window.localStorage.setItem(key, JSON.stringify(v));
      } catch (error) {
        console.log(error);
      }
    },
    [key, storedValue, setStoredValue]
  );

  const clearValue = useCallback(() => {
    setStoredValue(undefined);
    window.localStorage.clearItem(key);
  }, [key, setStoredValue]);

  return [storedValue, setValue, clearValue];
};
