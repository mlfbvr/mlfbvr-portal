import React from 'react';
import clsx from 'clsx';

import { LoginButton } from '@mlfbvr/frontend/auth/ui/login-button';
import { InferPropTypes } from '@mlfbvr/shared/ui/proptypes';

const propTypes = {};
const defaultProps = {};

export type HeaderProps = InferPropTypes<typeof propTypes, typeof defaultProps>;

export function HeaderBase() {
  return (
    <header
      className={clsx('relative z-10', 'border-b border-gray-200', 'font-sans')}
    >
      <div
        className={clsx(
          'flex items-center justify-between',
          'px-4 py-2',
          'sm:px-6',
          'lg:px-8'
        )}
      >
        <h1 className="font-semibold text-lg">Welcome to my portal!</h1>
        <LoginButton />
      </div>
    </header>
  );
}

HeaderBase.displayName = 'Header';
HeaderBase.propTypes = propTypes;
HeaderBase.defaultProps = defaultProps;

export const Header = React.memo(HeaderBase);
