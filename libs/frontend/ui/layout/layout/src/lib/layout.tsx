import React from 'react';
import { node } from 'prop-types';
import clsx from 'clsx';

import { Header } from '@mlfbvr/frontend/ui/layout/header';
import { InferPropTypes } from '@mlfbvr/shared/ui/proptypes';

const propTypes = {
  children: node,
};

const defaultProps = {
  children: null,
};

export type LayoutProps = InferPropTypes<typeof propTypes, typeof defaultProps>;

export const LayoutBase = ({ children }: LayoutProps) => (
  <>
    <Header />
    <main className={clsx('flex-auto flex-shrink-0', 'flex flex-col')}>
      {children}
    </main>
  </>
);

LayoutBase.displayName = 'Layout';
LayoutBase.propTypes = propTypes;
LayoutBase.defaultProps = defaultProps;

export const Layout = React.memo(LayoutBase);
