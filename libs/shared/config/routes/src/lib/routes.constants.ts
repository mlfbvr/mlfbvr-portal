export const AUTH_ROUTES = {
  LOGIN: '/auth/user/login',
  LOGOUT: '/auth/user/logout',
};
