import { compose, split, map } from 'lodash/fp';

export default {
  rabbitmq: {
    host: compose(
      map((host) => `amqp://${host}`),
      split(',')
    )(process.env.RABBITMQ_HOST || ''),
  },
};
