import * as Case from 'case';
import * as faker from 'faker';
import { getQueueEnvKey, getQueueName } from './queue';
import { getEnvironmentVariable } from '@mlfbvr/utils/env';

jest.mock('@mlfbvr/utils/env');

describe('@mlfbvr/shared/config/services/lib/config/queue', () => {
  describe('getQueueEnvKey', () => {
    it('should use the uppercase service name', () => {
      const serviceName = 'myservicename';
      expect(getQueueEnvKey(serviceName)).toEqual(
        expect.stringContaining('MYSERVICENAME')
      );
    });

    it('should return a SCREAMING_PASCAL_CASE value', () => {
      expect(Case.of(getQueueEnvKey(faker.random.word()))).toBe('constant');
    });
  });

  describe('getQueueName', () => {
    it('should return the environment variable', () => {
      const queueName = faker.random.word();
      (getEnvironmentVariable as jest.Mock).mockImplementationOnce(
        () => queueName
      );

      expect(getQueueName(faker.random.word())).toBe(queueName);
    });

    it('should return the service name in lowercase as a fallback value', () => {
      const serviceName = faker.random.word();
      (getEnvironmentVariable as jest.Mock).mockImplementationOnce(
        () => undefined
      );

      expect(getQueueName(serviceName)).toBe(serviceName.toLowerCase());
    });
  });
});
