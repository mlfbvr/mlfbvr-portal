import * as Case from 'case';
import { lowerCase } from 'lodash';
import { getEnvironmentVariable } from '@mlfbvr/utils/env';

export const getQueueEnvKey = (serviceName: string): string =>
  Case.constant(`${serviceName}_SERVICE_QUEUE`);

export const getQueueName = (serviceName: string): string =>
  getEnvironmentVariable(getQueueEnvKey(serviceName)) || lowerCase(serviceName);
