import { Transport, ClientProviderOptions } from '@nestjs/microservices';
import { keyBy } from 'lodash';

import config from './config/env';
import { getQueueName } from './config/queue';

export enum SERVICES_NAMES {
  AUTHENTICATION = 'AUTHENTICATION',
}

export const SERVICES: ClientProviderOptions[] = [
  {
    name: SERVICES_NAMES.AUTHENTICATION,
    transport: Transport.RMQ,
    options: {
      urls: config.rabbitmq.host,
      queue: getQueueName(SERVICES_NAMES.AUTHENTICATION),
      queueOptions: {
        durable: true,
      },
    },
  },
];

export const SERVICES_MAP: Record<
  SERVICES_NAMES,
  ClientProviderOptions
> = keyBy(SERVICES, 'name');
