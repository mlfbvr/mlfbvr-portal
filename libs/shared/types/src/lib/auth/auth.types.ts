export interface IUser {
  id: string;
}

export interface User extends IUser {
  firstName: string;
  lastName: string;
  email: string;
}

export interface ServiceAccount extends IUser {
  name: string;
}

export const isUser = (user: IUser): user is User => {
  return (user as User).lastName !== undefined;
};

export const isServiceAccount = (user: IUser): user is ServiceAccount => {
  return (user as ServiceAccount).name !== undefined;
};
