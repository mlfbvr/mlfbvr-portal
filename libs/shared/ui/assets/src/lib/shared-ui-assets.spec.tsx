import React from 'react';
import { render } from '@testing-library/react';

import SharedUiAssets from './shared-ui-assets';

describe('SharedUiAssets', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<SharedUiAssets />);
    expect(baseElement).toBeTruthy();
  });
});
