import React from 'react';

import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface SharedUiAssetsProps {}

const StyledSharedUiAssets = styled.div`
  color: pink;
`;

export function SharedUiAssets(props: SharedUiAssetsProps) {
  return (
    <StyledSharedUiAssets>
      <h1>Welcome to shared-ui-assets!</h1>
    </StyledSharedUiAssets>
  );
}

export default SharedUiAssets;
