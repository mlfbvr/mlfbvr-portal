import React from 'react';
import { func, oneOf, string, node } from 'prop-types';
import { noop } from 'lodash';

import { InferPropTypes } from '@mlfbvr/shared/ui/proptypes';
import { ButtonHTMLTypes, ButtonTypes } from './Button.types';

const basePropTypes = {
  type: oneOf(Object.values(ButtonTypes)),
  label: string,
  className: string,
  children: node,
};

const baseDefaultProps = {
  type: ButtonTypes.DEFAULT,
  label: '',
  className: '',
  children: null,
};

export type BaseButtonProps = InferPropTypes<
  typeof basePropTypes,
  typeof baseDefaultProps
>;

const nativePropTypes = {
  htmlType: oneOf(Object.values(ButtonHTMLTypes)),
  onClick: func,
};

const nativeDefaultProps = {
  htmlType: ButtonHTMLTypes.BUTTON,
  onClick: noop,
};

export type NativeButtonProps = {
  htmlType?: ButtonHTMLTypes;
  onClick?: React.MouseEventHandler<HTMLElement>;
};

export type ButtonProps = BaseButtonProps & NativeButtonProps;

export const propTypes = {
  ...basePropTypes,
  ...nativePropTypes,
};

export const defaultProps = {
  ...baseDefaultProps,
  ...nativeDefaultProps,
};
