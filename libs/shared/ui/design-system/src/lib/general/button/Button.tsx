import React from 'react';
import clsx from 'clsx';

import { ButtonTypes } from './Button.types';
import { ButtonProps, defaultProps, propTypes } from './Button.props';

const BUTTON_CLASSNAMES = {
  [ButtonTypes.PRIMARY]: [
    'bg-gradient-to-r from-purple-400 to-purple-500 text-white',
    'hover:from-purple-500 hover:to-purple-600 hover:text-gray-100',
    'focus:ring-2 focus:ring-purple-300 focus:ring-opacity-75',
  ],
  [ButtonTypes.DEFAULT]: [
    'bg-gradient-to-r from-purple-400 to-purple-500 text-white',
    'hover:from-purple-500 hover:to-purple-600 hover:text-gray-100',
    'focus:ring-2 focus:ring-purple-300 focus:ring-opacity-75',
  ],
};

export const ButtonBase = ({
  type,
  label,
  className,
  children,
  htmlType,
  ...nativeProps
}: ButtonProps) => (
  <button
    className={clsx(
      'flex items-center justify-center',
      'rounded-lg shadow-md',
      'px-4 py-2',
      ...[].concat(BUTTON_CLASSNAMES[type]),
      className
    )}
    type={htmlType}
    {...nativeProps}
  >
    {label || children}
  </button>
);

ButtonBase.displayName = 'Button';
ButtonBase.propTypes = propTypes;
ButtonBase.defaultProps = defaultProps;

export const Button = React.memo(ButtonBase);
