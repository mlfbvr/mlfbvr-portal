export enum ButtonTypes {
  PRIMARY = 'primary',
  DEFAULT = 'default',
  TEXT = 'dashed',
  LINK = 'link',
}

export enum ButtonHTMLTypes {
  SUBMIT = 'submit',
  BUTTON = 'button',
  RESET = 'reset',
}
