# shared-ui-proptypes

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test shared-ui-proptypes` to execute the unit tests via [Jest](https://jestjs.io).
