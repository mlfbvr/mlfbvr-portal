export default class MissingEnvironmentVariableError extends Error {
  constructor(environmentVariableKey: string) {
    super(`Missing ${environmentVariableKey}.`);
  }
}
