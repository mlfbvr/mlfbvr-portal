import {
  getEnvironmentVariable,
  MissingEnvironmentVariableError,
} from '@mlfbvr/utils/env';

describe('libs/utils/env', () => {
  describe('getEnvironmentVariable', () => {
    const envCopy = { ...process.env };

    afterAll(() => {
      process.env = { ...envCopy };
    });

    it.each`
      key       | value     | transform                 | expected
      ${'port'} | ${'9000'} | ${undefined}              | ${'9000'}
      ${'yolo'} | ${'swag'} | ${undefined}              | ${'swag'}
      ${'port'} | ${'9000'} | ${(v) => +v}              | ${9000}
      ${'yolo'} | ${'swag'} | ${(v) => v.toUpperCase()} | ${'SWAG'}
    `(
      'should transform the retrieved environment variable',
      ({ key, value, transform, expected }) => {
        process.env = {
          ...envCopy,
          [key]: value,
        };

        expect(getEnvironmentVariable(key, { transform })).toBe(expected);
      }
    );

    it('should throw an error if the environment variable was not specified and shouldThrow is true', () => {
      const key = 'yolo';

      process.env = {
        ...envCopy,
        [key]: undefined,
      };

      expect.assertions(2);

      try {
        getEnvironmentVariable(key, { shouldThrow: true });
      } catch (error) {
        expect(error instanceof MissingEnvironmentVariableError).toBe(true);
        expect(error.message).toEqual(expect.stringContaining(key));
      }
    });
  });
});
