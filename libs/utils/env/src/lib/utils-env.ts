import MissingEnvironmentVariableError from './error/MissingEnvironmentVariableError';

export { MissingEnvironmentVariableError };

export type GetEnvironmentVariableOptions<T> = {
  shouldThrow?: boolean;
  transform?: (string) => T;
};

export const getEnvironmentVariable = <T>(
  envKey: string,
  { shouldThrow, transform }: GetEnvironmentVariableOptions<T> = {}
) => {
  const value = process.env[envKey] || '';

  if (value === '' && shouldThrow) {
    throw new MissingEnvironmentVariableError(envKey);
  }

  if (transform) {
    return transform(value);
  }

  return value;
};
