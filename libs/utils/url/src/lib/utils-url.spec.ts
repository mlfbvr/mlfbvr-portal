import {
  hasProtocol,
  ensureProtocol,
  removePlaceholderProtocol,
  PLACEHOLDER_PROTOCOL,
} from './utils-url';

describe('libs/utils/url', () => {
  describe('hasProtocol', () => {
    it.each`
      url               | expected
      ${'http://yolo'}  | ${true}
      ${'https://yolo'} | ${true}
      ${'redis://yolo'} | ${true}
      ${'yolo'}         | ${false}
      ${'http//yolo'}   | ${false}
    `('should return if the url has a protocol or not', ({ url, expected }) => {
      expect(hasProtocol(url)).toBe(expected);
    });
  });

  describe('ensureProtocol', () => {
    it.each`
      url
      ${'http://yolo'}
      ${'https://yolo'}
      ${'redis://yolo'}
    `('should not edit the URL if protocol is present', ({ url }) => {
      expect(ensureProtocol(url)).toBe(url);
    });

    it.each`
      url
      ${'yolo'}
      ${'http//yolo'}
    `('should add a placeholder protocol if it was absent ', ({ url }) => {
      expect(ensureProtocol(url)).toEqual(
        expect.stringContaining(PLACEHOLDER_PROTOCOL)
      );
      expect(hasProtocol(ensureProtocol(url))).toBe(true);
    });
  });

  describe('removePlaceholderProtocol', () => {
    it.each`
      url               | expected
      ${'http://yolo'}  | ${'http://yolo'}
      ${'https://yolo'} | ${'https://yolo'}
      ${'redis://yolo'} | ${'redis://yolo'}
      ${'yolo'}         | ${'//yolo'}
      ${'http//yolo'}   | ${'//http//yolo'}
    `('should not edit the URL if protocol is present', ({ url, expected }) => {
      expect(removePlaceholderProtocol(ensureProtocol(url))).toBe(expected);
    });
  });
});
