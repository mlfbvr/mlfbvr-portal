export const HAS_PROTOCOL_REGEXP = /:\/\//;
export const PLACEHOLDER_PROTOCOL = 'placeholder-protocol:';
export const PLACEHOLDER_PROTOCOL_REGEXP = new RegExp(
  `^${PLACEHOLDER_PROTOCOL}`,
  'i'
);

export const hasProtocol = (url: string): boolean =>
  HAS_PROTOCOL_REGEXP.test(url);

export const appendPlaceholderProtocol = (url: string): string =>
  `${PLACEHOLDER_PROTOCOL}//${url}`;

export const removePlaceholderProtocol = (url: string): string =>
  url.replace(PLACEHOLDER_PROTOCOL_REGEXP, '');

export const ensureProtocol = (url: string): string =>
  hasProtocol(url) ? url : appendPlaceholderProtocol(url);
