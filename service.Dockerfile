FROM node:12 AS base

FROM base

ARG PACKAGE_PATH

WORKDIR /app
COPY ./dist/$PACKAGE_PATH /app
COPY ./yarn.lock /app/yarn.lock

RUN yarn \
  # My beloved NestJS listed those as peer dependencies but still needs them :)
  && yarn add reflect-metadata rxjs

CMD ["node", "main.js"]
