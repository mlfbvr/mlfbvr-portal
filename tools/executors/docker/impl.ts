import { join } from 'path';

import { CommandResponse } from '../../utils/types';
import { runCommand } from '../../utils/run';
import { closeFile, openFile, writeCommand } from '../../utils/write';

interface Options {
  image: string;
  dockerFile: string;
  registry: string;
  tags?: readonly string[];
  args?: Record<string, string>;
  push?: boolean;
  ci?: string;
}

const prependAndFlatten = <T>(
  array: T[],
  preprend: string,
  transform: (v: T) => string
): string[] =>
  array.reduce(
    (acc: string[], v: T) => acc.concat([preprend, transform(v)]),
    [] as string[]
  );

export default async ({
  image,
  registry,
  tags,
  dockerFile,
  args,
  push: shouldPush = false,
  ci,
}: Options): Promise<CommandResponse> => {
  const images = (tags || ['latest']).map(
    (tag) => `${registry}/${image}:${tag}`
  );

  console.info(`Building ${images.join(' ')}`);

  const buildArgs = [
    'build',
    '-f',
    dockerFile,
    // tags
    ...prependAndFlatten(images, '-t', (v) => v),
    // build args
    ...prependAndFlatten(
      Object.entries(args),
      '--build-arg',
      ([key, value]) => `${key}=${value}`
    ),
    '.',
  ];

  const pushArgs = ['push', ...images];

  if (ci) {
    console.info('Dry run : only writing to files');

    const directory = join(process.cwd(), ci);
    const filename = `${image}.sh`;

    return Promise.resolve(openFile(directory, filename))
      .then(writeCommand('#!/usr/bin/env', ['sh\n']))
      .then(writeCommand('docker', buildArgs))
      .then((buildResponse) =>
        !shouldPush || !buildResponse.success
          ? // If I should not push (or the image failed to build), stop right there
            buildResponse
          : // Else push
            writeCommand('docker', pushArgs)(buildResponse)
      )
      .then(closeFile);
  }

  return runCommand('docker', buildArgs).then((buildResponse) =>
    !shouldPush || !buildResponse.success
      ? // If I should not push (or the image failed to build), stop right there
        buildResponse
      : // Else push
        runCommand('docker', pushArgs)
  );
};
