import * as childProcess from 'child_process';
import { CommandResponse } from './types';

export const spyChildProcess = (
  child: childProcess.ChildProcessWithoutNullStreams
): Promise<CommandResponse> =>
  new Promise<CommandResponse>((resolve) => {
    child.stdout.on('data', (data) => console.info(data.toString()));
    child.stderr.on('data', (data) => console.error(data.toString()));

    child.on('close', (code) => {
      console.info('Done.');
      resolve({ success: code === 0 });
    });
  });

export const runCommand = (
  cmd: string,
  args: readonly string[]
): Promise<{ success: boolean }> => {
  console.info(`Running ${cmd} ${args.join(' ')}`);
  return spyChildProcess(childProcess.spawn(cmd, args));
};
