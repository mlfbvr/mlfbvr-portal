import { join } from 'path';
import { createWriteStream, WriteStream } from 'fs';
import { promisify } from 'util';

export const openFile = (directory, filename): { file: WriteStream } => {
  return { file: createWriteStream(join(directory, filename)) };
};

export const writeCommand = (cmd: string, args: readonly string[]) => async ({
  file,
}: {
  file: WriteStream;
}): Promise<{ file: WriteStream; success: boolean }> => {
  console.info(`Writing ${cmd} ${args.join(' ')}`);
  const write = promisify(file.write.bind(file));
  try {
    await write(`${[].concat(cmd).concat(args).join(' ')}\n`);
    return { file, success: true };
  } catch (err) {
    console.error(err);
    return { file, success: false };
  }
};

export const closeFile = ({
  file,
  success,
}: {
  file: WriteStream;
  success: boolean;
}): { success: boolean } => {
  file.close();
  return { success };
};
